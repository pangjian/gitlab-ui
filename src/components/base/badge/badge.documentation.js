import description from './badge.md';

export default {
  description,
  followsDesignSystem: true,
};
